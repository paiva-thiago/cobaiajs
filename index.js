const nodemailer = require('nodemailer');
const {p} = require('./properties.js')
const dotenv = require('dotenv')
dotenv.config()

const SOURCE = 'thiagopaiva.me'

var prp   = p[SOURCE].prp
var smtp  = p[SOURCE].smtp


let formatado ="<h1>oi</h1>"
let assunto ="Email enviado?"
let texto ="oi"


let smtpTransport = nodemailer.createTransport(smtp);


console.log("SMTP HOST " + process.env.EMAIL_HOST)

// setup e-mail data with unicode symbols
var mailOptions = {
    from: prp.de.concat(' <').concat(prp.from).concat('>'),//'Thiago Paiva <mailer@thiagopaiva.com>', // sender address
    replyTo: prp.replyTo,
    to:  prp.to, 
    cc:  prp.cc, 
    subject: assunto, // Subject line
    text: texto, 
    html: formatado // html body
}

// send mail with defined transport object
smtpTransport.sendMail(mailOptions, function(error, response){
    if(error){
        console.log(error);
    }else{
        console.log('Processo concluído!');
    }

    // if you don't want to use this transport object anymore, uncomment following line
    smtpTransport.close(); // shut down the connection pool, no more messages
});