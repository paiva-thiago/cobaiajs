const _prp = function (by,from){
    return {
        "de"            :  "Chat do CRECISP - POC by ".concat(by),
        "from"          :  from,
        "replyTo"       :  "pste13.informatica@crecisp.gov.br",    
        "to"            :  "paiva.thiago@gmail.com"
    }
}

const _smtp = function (theHost, thePort, isSecure, authUser,authPass){
    return {
        "host": theHost,
        "port": thePort,
        "secure": isSecure,
        "auth": {
            "user": authUser,            
            "pass": authPass
        }
    }
};

const _account = function(by,from,theHost, thePort, isSecure, authUser,authPass){
    return {
        "smtp":_smtp(theHost, thePort, isSecure, authUser,authPass),
        "prp":_prp(by,from)
    }
}

const prps = {
    "thiagopaiva.me":_account("thiagopaiva.me","mailer@thiagopaiva.me","smtp.umbler.com", "587", false, "mailer@thiagopaiva.me","NaoSei20Vezes!"),
    "mailgun":_account("mailgun","naoresponda@crecisp.gov.br","smtp.mailgun.org", "587", false, "postmaster@sandboxdd699f7c917644198fb7c0e247bec092.mailgun.org","e475acd6bd873f958bbc4c4c310ebb5b-6ae2ecad-e66b5b6c")    
}
module.exports={p:prps}